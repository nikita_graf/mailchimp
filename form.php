<?php
define("MAILCHIMP_API_KEY", "a832502f779a0125a2710f64c7fff9de-us3");

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/MailchimpListRouter.php';
use Respect\Validation\Validator as v;


$router = new MailchimpListRouter(MAILCHIMP_API_KEY);
$router->handle(array(
    'name' => 'downloadSoftware',
    'listId' => '3816448f4e',
    'validation' => array(
        'FNAME' => v::string()->notEmpty(),
        'LNAME' => v::string()->notEmpty(),
        'MMERGE3' => v::string()->notEmpty(), //Title
        'MMERGE4' => v::string()->notEmpty(), //Company
        'EMAIL' => v::email()->notEmpty()
    ),
    'success' => function() {
        return 'http://www.datometry.com/downloads0f010400-eb91baca-16244cb5-dbc6b7b3-7a3251b9-5e18532c.html';
    }
));
$router->handle(array(
    'name' => 'downloadPaper',
    'listId' => '1af5b3b095',
    'validation' => array(
        'FNAME' => v::string()->notEmpty(),
        'LNAME' => v::string()->notEmpty(),
        'MMERGE3' => v::string()->notEmpty(), //Title
        'MMERGE4' => v::string()->notEmpty(), //Company
        'EMAIL' => v::email()->notEmpty()
    ),
    'success' => function() {
        return 'http://www.datometry.com/files/theme/Datometry_FinSvcs_Paper.pdf';
    }
));
$router->handle(array(
    'name' => 'requestDemo',
    'listId' => '57276100de',
    'validation' => array(
        'FNAME' => v::string()->notEmpty(),
        'LNAME' => v::string()->notEmpty(),
        'EMAIL' => v::email()->notEmpty()
    ),
    'success' => function() {
        return '';
    }
));
$router->run($_GET['action']);