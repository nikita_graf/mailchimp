<?php
if (ob_get_level()) {
    ob_end_clean();
}

$file = $_GET['file'];

if (!preg_match("/^http(s)?:\/\//i", $file) || preg_match("/\.\./i", $file)) {
    exit;
}

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . basename($file));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');

if ($fd = fopen($file, 'rb')) {
    while (!feof($fd)) {
        print fread($fd, 1024);
    }
    fclose($fd);
}
exit;