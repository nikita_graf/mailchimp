jQuery(function ($) {
  var ua = navigator.userAgent.toLowerCase();
  var downloadFile = function(url, downloadHost) {
    console.log()
    if (document.createEvent && ua.indexOf('firefox') == -1 && !("ActiveXObject" in window)) {
      var link = document.createElement('a');
      var fileName = url.substring(url.lastIndexOf('/') + 1, url.length);
      var e = document.createEvent('MouseEvents');

      link.setAttribute("target","_blank");
      link.href = url;

      if (link.download !== undefined) {
        link.download = fileName;
      }

      e.initEvent('click', true ,true);
      link.dispatchEvent(e);
      return true;
    }

    location.href = downloadHost + '?file='  + url;
  };

  function createDgForm(selector, opts) {
    $(selector).each(function () {
      var options = opts || {};
      var form = $(this);
      var fieldErrorSelector = '.dg-form__field-error';
      var formMessageboxSelector = '.dg-form__message';
      var formMessagebox = form.find(formMessageboxSelector);
      var submitButton = form.find('input[type="submit"]');
      var popupContainer = form.parentsUntil('.mfp-content');
      var popupCloseButton = popupContainer.find('.mfp-close');

      popupCloseButton.click(resetForm);
      submitButton.removeAttr('disabled');

      function closeModal() {
        popupCloseButton.trigger('click');
      }

      function resetFieldErrors() {
        form.find(fieldErrorSelector).remove();
      }

      function resetFields() {
        form.get(0).reset();
      }

      function resetForm() {
        resetFieldErrors();
        form.find(formMessagebox).empty().hide();
        resetFields();
      }

      function showMessage(messageType, message) {
        var className = formMessageboxSelector.slice(1);
        var appendMethod = typeof message === 'string' ? 'html' : 'append';

        formMessagebox
          .attr('class', className)
          .addClass(className + '_' + messageType)
          [appendMethod](message)
          .slideDown();
      }

      function successMessage(message) {
        showMessage('success', message)
      }

      function errorMessage(message) {
        showMessage('error', message)
      }

      function showFieldError(fieldName, message) {
        var field = form.find('[name="' + fieldName + '"]');
        var fieldParent = field.parent();
        var messageBox = fieldParent.find(fieldErrorSelector);

        if (!messageBox.length) {
          messageBox = $('<div></div>', {
            class: fieldErrorSelector.slice(1)
          })
            .appendTo(fieldParent);
        }

        messageBox.text(message);
      }

      form.submit(function (e) {
        e.preventDefault();

        var url = form.attr('action');
        var formData = form.serialize();

        submitButton.attr('disabled', 'disabled');

        $.post(url, formData)
          .then(function (response) {
            resetForm();
            (options.success || $.noop)(response, successMessage, closeModal);

            if (options.closeModalOnSuccess) {
              closeModal();
            }
          })
          .fail(function (response) {
            var error = {};

            try {
              error = JSON.parse(response.responseText);
            } catch(e) {}

            resetFieldErrors();

            if (error.fields) {
              $.each(error.fields, function(field, message) {
                showFieldError(field, message)
              });
            } else if (error.message) {
              errorMessage(error.message);
            } else {
              errorMessage('Unknown error. Please try again later.');
            }
          })
          .always(function () {
            submitButton.removeAttr('disabled');
          });
      });
    });
  }

  createDgForm('.download-software-form', {
    success: function(response, showMessage, closePopup) {
      var msg =
        '<span>Thank you for submitting your information. Please </span>' +
        '<a href="' + response.message +'" target="_blank">download</a>' +
        '<span> the software.</span>';

      msg = $(msg);
      msg.filter('a').one('click', closePopup);
      showMessage(msg);
    }
  });

  createDgForm('.download-paper-form', {
    closeModalOnSuccess: true,
    success: function(response, showMessage, closePopup, openUrl) {
      downloadFile(response.message, 'http://mailch/download.php');
    }
  });

  createDgForm('.request-demo-form', {
    success: function(response, showMessage, closePopup, openUrl) {
      showMessage(
        'Thank you for requesting a demo of Datometry Hyper-Q Enterprise Edition. ' +
        'We will contact you shortly to schedule a convenient time'
      );
    }
  });
});