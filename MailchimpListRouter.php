<?php
use Respect\Validation\Exceptions\ValidationExceptionInterface;

class MailchimpListRouter {

    private $routes = array();
    private $apiKey = '';
    private $mailchimpApiUrl;

    public function __construct($apiKey) {
        preg_match('/-(.*?)$/', $apiKey, $matches);
        $server = $matches[1];
        $this->apiKey = $apiKey;
        $this->mailchimpApiUrl = "https://$server.api.mailchimp.com/3.0";
    }

    public function handle($route) {
        $this->routes[] = (object)$route;
    }

    private function successResponse($message) {
        $this->sendResponse(200, json_encode(array(
            'message' => $message
        )));
    }

    private function errorResponse($error) {
        $this->sendResponse(400, json_encode(
            is_string($error)
                ? array('message' => $error)
                : $error
        ));
    }

    private function sendResponse($status, $body) {
        header("HTTP/1.1 $status");
        header('Content-Type: application/json');
        header("Access-Control-Allow-Origin: *");
        echo $body;
    }

    private function getEmailMd5($email) {
        return md5(strtolower($email));
    }

    private function transformRequest($request) {
        $req = clone $request;
        unset($req->EMAIL);

        return array(
            'email_address' => $request->EMAIL,
            'status' => 'subscribed',
            'merge_fields' => $req
        );
    }

    private function runRoute($route) {
        $request = (object)$_POST;
        $invalidParam = null;
        $errors = array();

        foreach($route->validation as $param => $validator) {
            try {
                $validator->check($request->{$param});
            } catch(ValidationExceptionInterface $exception) {
                $errors[$param] = preg_replace('/^".*?"\s/', '', $exception->getMainMessage());
            }
        }

        if (count($errors) > 0) {
            return $this->errorResponse(array(
                'fields' => $errors
            ));
        }

        $email = $request->EMAIL;
        $apiMethod = "/lists/{$route->listId}/members/";
        $mcResponse = \Httpful\Request::post($this->mailchimpApiUrl . $apiMethod)
            ->authenticateWith('username', $this->apiKey)
            ->sendsJson()
            ->body(json_encode($this->transformRequest($request)))
            ->send();

        $mcResponseBody = $mcResponse->body;

        if (
            $mcResponseBody->status == 400 && $mcResponseBody->title == 'Member Exists'
            || $mcResponseBody->status == 'subscribed'
        ) {
            $response = call_user_func_array($route->success, array());
            $this->successResponse($response);
        } else if ($mcResponseBody->title == 'Invalid Resource') {
            $this->errorResponse($mcResponseBody->detail);
        } else {
            $this->errorResponse('Unknown error. Please try again later.');
        }
    }

    function run($action) {
        foreach($this->routes as $route) {
            if ($route->name === $action) {
                $this->runRoute($route);
                return;
            }
        }

        $this->errorResponse('Unknown error. Please try again later.');
    }
}






